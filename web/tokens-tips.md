# JWT
JSON Web Token (JWT) is an open standard (RFC 7519) that defines a compact and self-contained way for securely transmitting information between parties as a JSON object.  
**One can send authentication information via these tokens.**

A JWT typically looks like the following.

`xxxxx.yyyyy.zzzzz`

*These can look like base64 encoded strings.*

Website to [debug](https://jwt.io/) JWT tokens.
