# Articles
https://wixnic.github.io/posts/htb-traverxec/#privilege-escalaton-www-data--david

https://book.hacktricks.xyz/

# Automatic reconnaissance
[LinPEAS](https://github.com/carlospolop/PEASS-ng)

# Chisel, a fast proxy
https://0xdf.gitlab.io/2020/08/10/tunneling-with-chisel-and-ssf-update.html
